<?php

    $title = 'Russell Bishop - Frontend UX Designer';

    $description = 'UX &amp; Strategy, UI Design and Frontend Templating';

    
    require 'vendor/autoload.php';
    include('src/dochead.php');
    
  ?>

<body class="c-home-feed">

<?php 

use \TANIOS\Airtable\Airtable;
$airtable = new Airtable(array(
    'api_key' => 'keyll8LpyyN3adHhR',
    'base'    => 'appCUlgmIAFG4aaRd'
));

$request = $airtable->getContent( 'Articles' );

do {
    $response = $request->getResponse();
    var_dump( $response[ 'records' ] );
}
while( $request = $response->next() );

// print_r($request);

print_r($request[0])

?>

<main class="c-poster">

  <header class="c-poster__headings">
    <h1 class="c-poster__name">Russell Bishop</h1>
    <h2 class="c-poster__title">Frontend UX Designer</h2>
  </header>

  <dl class="c-poster__skills">
    <dt class="o-dictate">Skills</dt>
    <dd class="c-poster__skill">UX &amp; Strategy</dd>
    <dd class="c-poster__skill">UI Design</dd>
    <dd class="c-poster__skill">Frontend Templating</dd>
  </dl>

  <dl class="c-poster__networks">
    <dt class="o-dictate">Contact methods</dt>
    <dd><a class="c-poster__network-link" href="http://twitter.com/RussellBishop">
      <span class="o-dictate">Twitter</span>
      <svg class="c-poster__network-icon" viewBox="0 0 30 30">
        <use xlink:href="/dist/svg/sprite.svg#twitter-blank"></use>
      </svg>
      <svg class="c-poster__network-icon c-poster__network-icon--focus" viewBox="0 0 30 30">
        <use xlink:href="/dist/svg/sprite.svg#twitter-blank"></use>
      </svg>
    </a></dd>
    <dd><a class="c-poster__network-link" href="mailto:hey@russellbishop.co.uk">
      <span class="o-dictate">Email</span>
      <svg class="c-poster__network-icon" viewBox="0 0 30 30">
        <use xlink:href="/dist/svg/sprite.svg#email-blank"></use>
      </svg>
      <svg class="c-poster__network-icon c-poster__network-icon--focus" viewBox="0 0 30 30">
        <use xlink:href="/dist/svg/sprite.svg#email-blank"></use>
      </svg>
    </a></dd>
    <dd><a class="c-poster__network-link" href="http://dribbble.com/RussellBishop">
      <span class="o-dictate">Dribbble</span>
      <svg class="c-poster__network-icon" viewBox="0 0 30 30">
        <use xlink:href="/dist/svg/sprite.svg#dribbble-blank"></use>
      </svg>
      <svg class="c-poster__network-icon c-poster__network-icon--focus" viewBox="0 0 30 30">
        <use xlink:href="/dist/svg/sprite.svg#dribbble-blank"></use>
      </svg>
    </a></dd>
  </dl>

  <div class="c-poster__shade"></div>

  <div class="c-poster__portrait">
    <img class="c-poster__image"
      src="/dist/img/portrait@1004x1280h.jpg"
      srcset="/dist/img/portrait@251x320h.jpg 251w,
        /dist/img/portrait@502x640h.jpg 502w,
        /dist/img/portrait@753x960h.jpg 753w,
        /dist/img/portrait@1004x1280h.jpg 1004w"
      alt="Portrait photograph of Russell Bishop wearing a shirt, stood in front of a grey backdrop and lit from one side by an orange light. He wishes he could go back and iron his shirt more thoroughly for the photoshoot."
     />
  </div>

  <div class="c-poster__slant">
    <svg aria-hidden="true" class="c-poster__svg" xmlns="http://www.w3.org/2000/svg" width="388" height="263" viewBox="0 0 388 263">
      <path d="m101 495h288l-254.68841 240.903728c-31.703203 29.987308-81.7132439 28.5963-111.7005523-3.106903-13.87792987-14.672035-21.6110377-34.101144-21.6110377-54.296825v-83.5c0-55.228475 44.771525-100 100-100z" opacity=".64" transform="matrix(-1 0 0 1 389 -495)"/>
    </svg>
  </div>

</main>

<?php

    include('components/content-feed.php');
    
  ?>

<script type="text/javascript">

function offset(el) {
  var rect = el.getBoundingClientRect(),
  scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
  scrollTop = window.pageYOffset || document.documentElement.scrollTop;
  return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

// example use
var contentFeed = document.querySelector('.c-content-feed');
var contentFeedOffset = offset(contentFeed);
console.log(contentFeedOffset.top);

if(contentFeedOffset.top === 0) {
  document.body.classList.add("is-feed-top");
  console.log('added');
} else {
  document.body.classList.remove("is-feed-top");
  console.log('removed');
}

</script>

</body>
</html>
