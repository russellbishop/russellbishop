<?php

    $title = 'Container Units – #frontend #css';
    $description = 'Container units are a specialized set of CSS variables that allow you to build grids, layouts, and components using columns and gutters.';

    $hasCode = true;

    include('../src/dochead.php'); 

    $codepenHeight = 470;
?>

<body class="u-fd-column">

<nav class="c-back">
    <div class="o-container u-flex">
        <a class="c-back__link" href="/">&larr; Back to home</a>
    </div>
</nav>

<header class="c-header c-header--container-units o-article-header">

    <?php include('../components/headers/container-units-demo.php'); ?>

    <div class="c-header__title">
        <h1 class="c-header__name o-type-h0">Container Units</h1>

        <div class="c-header__tags u-flex u-ai-center">
            <dd>CSS Documentation</dd>
        </div>
    </div>
</header>

<nav class="c-progress">
    <div class="o-container">
        <div class="c-progress__screen">
            <?php /* 
            <p class="o-ellipsis o-type-16px">Introduction</p>
            
            <div class="u-flex u-ai-center">
                <span>1 of 6</span>
                <div class="u-flex u-fd-column">
                    <a disabled href="#what-can-container-units-do-for-me">^<span class="o-dictate">Up one section</span></a>
                    <a href="#why-are-container-units-different">v<span class="o-dictate">Next section</span></a>
                </div>
            </div>
            */ ?>
        </div>
    </div>
</nav>

<main>

    <article class="c-article o-container o-article-section">

        <nav class="c-menu o-article-section__aside u-as-flex-start u-mt-siblings-s">

            <ol>
                <li><a href="#introduction">Introduction</a></li>
                <li><a href="#demo">Demo</a></li>
                <li><a href="#download">Download</a></li>
            </ol>

            <ol>
                <li><a href="#display-grid">Use with <code>display: grid</code></a></li>
                <li><a href="#display-flex">Use with <code>display: flex</code></a></li>
            </ol>

            <ol>
                <li><a href="#gutters-outside">Gutters on the outside</a></li>
                <li><a href="#scrollbars-100vw">Scrollbars and <code>100vw</code></a></li>
                <!-- <li><a href="#responsive-grid-settings">Responsive Grid Settings</a></li> -->
                <!-- <li><a href="#scss-functions">SCSS functions</a></li> -->
            </ol>

        </nav>

        <section id="introduction" class="u-mt-siblings-m">

            <div class="s-article">
                <h1>Introduction</h1>

                <p>Container units are a specialized set of CSS variables that allow you to build grids, layouts, and components using columns and gutters.</p>

                <p>They mirror the layout functionality found in UI design software where configuring just three values provides your document with a global set of columns and gutters to measure and calculate from.</p>

            </div>
            
            <p>
                <a class="c-external-link c-external-link--smashing" href="https://www.smashingmagazine.com/2019/03/robust-layouts-container-units-css/">
                    <span class="c-external-link__text">Missed the intro? Start with the article on </span> 
                    <span title="Smashing Magazine" class="c-external-link__logo-base"><img class="c-external-link__logo-image" src="/dist/img/smashing-magazine.png" /></span>
                </a>
            </p>

        </section>

        <section id="demo">

            <div class="s-article">

                <h1>Demo</h1>

                <div class="o-oversized">

                    <p class="codepen" data-height="<?=$codepenHeight?>" data-theme-id="dark" data-default-tab="css,result" data-user="russelllighthouse" data-slug-hash="RdZarV" style="height: <?=$codepenHeight?>px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid black; margin: 1em 0; padding: 1em;" data-pen-title="Container Units - Gist Demo">
                    <span>See the Pen <a href="https://codepen.io/russelllighthouse/pen/RdZarV/"> Container Units - Gist Demo</a> by Russell Bishop (<a href="https://codepen.io/russelllighthouse">@russelllighthouse</a>) on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>

                </div>

            </div>

        </section>

        <section id="download">

            <div class="s-article">

                <h1>Download</h1>

                <!-- <script async src="https://gist.github.com/RussellBishop/4f6ed077878a9c15f76de41c201c4e5d.js"></script> -->
                <script src="https://gist.github.com/RussellBishop/4f6ed077878a9c15f76de41c201c4e5d.js"></script>
            </div>

        </section>

        <section id="display-grid">

            <div class="s-article">

                <h1>Use with <code>display: grid</code></h1>

                <p>First off, let&rsquo;s start with the most 'normal' interpration. This is how most instances of a 'grid container' are used – a big <code class="language-html">div</code> high up in the document.</p>
                
                <h2>Full Container</h2>

                <p>Some layouts may only ever use a grid container once – so here is everthing you'll need:</p>

                <?php include('code/grid-example.php'); ?>

                <h2>Partial grid container</h2>

                <p>When you want to use your grid measurements in a grid that isn't the full width of a container. This one's really easy, too.</p>

                <?php include('code/partial-grid-example.php'); ?>

            </div>

        </section>

        <section id="display-flex">

            <div class="s-article">

                <h1>Use with <code>display: flex</code></h1>

                <h3>Flex Grids</h3>

                <p>This example is set up for instances where you can't know will be inside the grid – I've added some pseudo-random <code class="language-css">:nth()</code> selectors to simulate this.</p>

                <p>Note that <code class="language-css">flex-grow: 0;</code> is set on the child elements. This is a stylistic choice – as I don't want each row to stretch and fill all available space in this example.</p>

                <div class="o-oversized">

                    <p class="codepen" data-height="<?=$codepenHeight?>" data-theme-id="dark" data-default-tab="css,result" data-user="russelllighthouse" data-slug-hash="NJoZra" style="height: <?=$codepenHeight?>px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid black; margin: 1em 0; padding: 1em;" data-pen-title="Container Units - Flex Demo">
                    <span>See the Pen <a href="https://codepen.io/russelllighthouse/pen/NJoZra/"> Container Units - Flex Demo</a> by Russell Bishop (<a href="https://codepen.io/russelllighthouse">@russelllighthouse</a>) on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>

                </div>

            </div>

        </section>

        <section id="gutters-outside">

            <div class="s-article">

                <h1>Use with Gutters on the Outside</h1>

                <p>The default 'gutters inside' setup for Container Units may not suit the design that you're recreating. Here's how you can configure variations with different gutters:</p>

                <h2>Gutters outside (half width)</h2>

                <?php include('code/gutters-outside-half.php'); ?>

                <h2>Gutters outside (normal width)</h2>

                <?php include('code/gutters-outside-normal.php'); ?>

            </div>

        </section>

        <section id="scrollbars-100vw">

            <div class="s-article">

                <h1>Scrollbars and 100vw</h1>

                <p>Unfortunately for developers everywhere, there is a quirk to using viewport units that we have to work around. The scenario is:</p>

                <ul>
                <li>The height of the document is greater than the device's viewport height, causing a vertical scrollbar</li>
                <li>The operating system has a scrollbar that is not transparent</li>
                <li>Your container width is above ~95vw at some breakpoints</li>
                </ul>

                <p>Because browsers calculate 100vw as <em>including</em> the scrollbar, your 100vw-wide content will actually slide <em>underneath</em> your scrollbar and be invisible. This is not only unexpected behaviour (but apparently, correct?!), but tricky to overcome. But we can!</p>

                <h2>Javascript to the rescue</h2>

                <p>The talented <a href="https://github.com/burntcustard">John Evans (@burntcustard)</a> has provided an excellent bit of javascript to measure <code class="language-css">var(--scrollbar-width)</code>.</p>

                <div class="o-oversized">

                    <p class="codepen" data-height="<?=$codepenHeight?>" data-theme-id="dark" data-default-tab="css,result" data-user="russelllighthouse" data-slug-hash="vPMpRe" style="height: <?=$codepenHeight?>; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid black; margin: 1em 0; padding: 1em;" data-pen-title="Container Units - Scrollbar Fixed">
                    <span>See the Pen <a href="https://codepen.io/russelllighthouse/pen/vPMpRe/">
                    Container Units - Scrollbar Fixed</a> by Russell Bishop (<a href="https://codepen.io/russelllighthouse">@russelllighthouse</a>)
                    on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>

                </div>

            </div>

        </section>

    </article>

</main>


<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

</body>
</html>
