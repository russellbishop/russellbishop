<pre class="c-code">
<code class="language-css"><?php echo htmlspecialchars('.o-partial-grid {
  width: columnspans(5); /* 5 columns + 4 gutters */

  display: grid;
  grid-template-columns: repeat(5, var(--column-unit)); 
  grid-column-gap: var(--gutter-unit);
}'); ?></code>
</pre>