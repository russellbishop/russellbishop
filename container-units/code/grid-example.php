<pre class="c-code">
<code class="language-css"><?php echo htmlspecialchars('.o-grid {
  width: var(--container-width);

  display: grid;
  grid-template-columns: repeat(var(--grid-columns), var(--column-unit)); 
  grid-column-gap: var(--gutter-unit);
}'); ?></code>
</pre>