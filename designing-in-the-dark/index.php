<?php

    $title = 'Designing in the dark – #design #stories';
    $description = ' ';

    include('../src/dochead.php'); 
?>

<body class="u-fd-column">

<nav class="c-back">
    <div class="o-container u-flex">
        <a class="c-back__link" href="/">&larr; Back to home</a>
    </div>
</nav>

<header class="c-header c-header--container-units o-article-header">

    <div class="c-header__title">
        <h1 class="c-header__name o-type-h0">Designing in the dark</h1>

        <div class="c-header__tags u-flex u-ai-center">
            <dt class="o-dictate">Categories:</dt>
            <dd><span aria-hidden="true">#</span>design</dd>
            <dd><span aria-hidden="true">#</span>stories</dd>
        </div>
    </div>
</header>

<nav class="c-progress">
    <div class="o-container">
        <div class="c-progress__screen">
            <?php /* 
            <p class="o-ellipsis o-type-16px">Introduction</p>
            
            <div class="u-flex u-ai-center">
                <span>1 of 6</span>
                <div class="u-flex u-fd-column">
                    <a disabled href="#what-can-container-units-do-for-me">^<span class="o-dictate">Up one section</span></a>
                    <a href="#why-are-container-units-different">v<span class="o-dictate">Next section</span></a>
                </div>
            </div>
            */ ?>
        </div>
    </div>
</nav>

<main>

    <article class="c-article o-container o-article-section">

        <nav class="c-menu o-article-section__aside u-as-flex-start u-mt-siblings-s">

            <ol>
                <li><a href="#introduction">Introduction</a></li>
                <li><a href="#demo">Demo</a></li>
                <li><a href="#download">Download</a></li>
            </ol>

            <ol>
                <li><a href="#display-grid">Use with <code>display: grid</code></a></li>
                <li><a href="#display-flex">Use with <code>display: flex</code></a></li>
            </ol>

            <ol>
                <li><a href="#gutters-outside">Gutters on the outside</a></li>
                <li><a href="#scrollbars-100vw">Scrollbars and <code>100vw</code></a></li>
                <!-- <li><a href="#responsive-grid-settings">Responsive Grid Settings</a></li> -->
                <!-- <li><a href="#scss-functions">SCSS functions</a></li> -->
            </ol>

        </nav>

        <section id="introduction" class="u-mt-siblings-m">

            <div class="s-article">
                <h1>How did I find myself here?</h1>

                <p>On one of my earlier clients at Lighthouse London tasked us with redesigning a growing blogging site aimed at parents who were 'Up All Hours' of the night.</p>

                <p>We set out to find some helpful insight that could steer our redesign – interviewing parents with newborns and young children who fit snugly into our target audience.</p>

                <p>Part of the exercise here is about trying to understand the context of how and where users will be interacting with the finished product; what time is it, what room are they in, how long will they be there, are they stressed… each of these can provide a signpost for how we can best accommodate for a user's needs.</p>

                <blockquote>
                It was after about the 3rd user interview that I realised I was going to design this platform with the lights turned off.
                </blockquote>

                <p>Every parent we spoke to described waking up in the middle of the night, huddling with their baby on a settee nearby and, with a bubbie nestled into their arm, would begin to squint at their bright phone screen and scroll some (self-professed!) trashy internet content.</p>

            </div>

        </section>

        <section id="display-grid">

            <div class="s-article">

                <h1>Use with <code>display: grid</code></h1>

                <p>First off, let&rsquo;s start with the most 'normal' interpration. This is how most instances of a 'grid container' are used – a big <code class="language-html">div</code> high up in the document.</p>
                
                <h2>Full Container</h2>

                <p>Some layouts may only ever use a grid container once – so here is everthing you'll need:</p>

                <h2>Partial grid container</h2>

                <p>When you want to use your grid measurements in a grid that isn't the full width of a container. This one's really easy, too.</p>

            </div>

        </section>

    </article>

</main>


</body>
</html>
