<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131657603-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-131657603-1');
    </script>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#F7C257">

    <title><?=$title?></title>
    <meta name="Description" content="<?=$description?>">

    <link rel="stylesheet" type="text/css" href="/dist/css/style.css">

    <?php if ($hasCode) {
        echo '
            <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro:400,700" rel="stylesheet">
            <script src="/assets/js/prism.js"></script>
        ';
    } ?>

    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
</head>