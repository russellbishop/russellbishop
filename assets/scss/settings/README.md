# Settings
- SCSS Variables, created to feed into the rest of your codebase.
- CSS Variables, created on `:root`, for your other elements to utilise.

```
$grid-max: 1680px;
```

```
$type-margin-top: 1.125rem;

:root {
  --typeMarginTop: $type-margin-top;
}
```
