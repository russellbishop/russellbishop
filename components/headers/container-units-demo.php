<div class="c-header__demo c-container-units-demo">

    <?php for ($k = 0 ; $k < 12; $k++) : ?>

        <div class="c-container-units-demo__column">

            <div class="c-container-units-demo__span" style="animation-delay: <?= $k * .125 ?>s"></div>

        </div>

    <?php endfor; ?>

</div>
