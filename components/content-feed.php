<?php

  $feed = array(

    // array(
      // TO ADD

      // Bandit Match

      // Spacing Library
    // ),

    array(
      'title' => 'Designing for the Future: Pattern Libraries',
      'url' => 'https://wearelighthouse.com/blog/designing-future-pattern-libraries/',
      'meta_key' => 'Design Podcast',
      'meta_description' => 'for Lighthouse London',
      'img' => '/dist/img/thumbnail/josh-russ.jpg',
      'img_alt' => 'Russell Bishop and Josh Ellis sitting down to record a design podcast',
    ),

    array(
      'title' => 'Building Robust Layouts with Container Units',
      'url' => 'https://www.smashingmagazine.com/2019/03/robust-layouts-container-units-css/',
      'meta_key' => 'CSS Article',
      'meta_description' => 'for Smashing Magazine',
      'img' => '/dist/img/thumbnail/container-units-bounce.gif',
      'img_alt' => 'Components for a website built using Container Units'
    ),

    array(
      'title' => 'Monzo + Zero-Sum Budgeting',
      'url' => 'https://airtable.com/universe/expainoj6mTURBb6y/monzo-zero-sum-budgeting',
      'meta_key' => 'Airtable Tool',
      'meta_description' => 'for personal financial budgeting',
      'img' => '/dist/img/thumbnail/monzo-airtable.png',
      'img_alt' => 'Monzo and Airtable logos',
    ),

    array(
      'title' => 'Weighting and Rating – How To Prioritise Your Backlog',
      'url' => 'https://wearelighthouse.com/blog/weighting-and-rating-how-to-prioritise-your-backlog/',
      'meta_key' => 'Product Podcast',
      'meta_description' => 'for Lighthouse London',
      'img' => '/dist/img/thumbnail/tom-russ.jpg',
      'img_alt' => 'Russell Bishop and Tom Johnson Ellis sitting down to record a product podcast',
    ),
    
    array(
      'title' => 'CSS Container Units',
      'url' => '/container-units',
      'meta_key' => 'CSS Documentation',
      'meta_description' => 'for a layout tool',
      'img' => '/dist/img/thumbnail/container-units.svg',
      'img_alt' => 'Demonstration of Container Units to build a grid',
    ),

    array(
      'title' => 'Utility Classes: Your library of extendable styles',
      'url' => 'https://blog.logrocket.com/css-utility-classes-library-extendable-styles/',
      'meta_key' => 'CSS Article',
      'meta_description' => 'for LogRocket',
      'img' => '/dist/img/thumbnail/utility-classes.gif',
      'img_alt' => 'A library of property: value pairs that power all your styles, managed from a single directory.'
    ),

    

    

    array(
      'title' => 'Cursing is Caring – Turning Frustration into Features',
      'url' => 'https://wearelighthouse.com/blog/cursing-is-caring-turning-frustration-into-features/',
      'meta_key' => 'Product Article',
      'meta_description' => 'for Lighthouse London',
      'img' => '/dist/img/thumbnail/cursing-caring.jpg',
      'img_alt' => 'Christy Quinn and Russell Bishop recording a podcast with two microphones in front of them.'
    ),
    
    
    
    
    array(
      'title' => 'Exploring a New Market with a Lean MVP',
      'url' => 'https://wearelighthouse.com/blog/one-step-at-a-time-exploring-a-new-market-with-a-lean-mvp/',
      'meta_key' => 'Product Podcast',
      'meta_description' => 'for Lighthouse London',
      'img' => '/dist/img/thumbnail/russ-christy.jpg',
      'img_alt' => 'Christy Quinn and Russell Bishop recording a podcast with two microphones in front of them.'
    ),

    array(
      'title' => 'Content Managed Responsive Wireframes',
      'url' => 'https://wearelighthouse.com/our-work/livewire/',
      'meta_key' => 'Product',
      'meta_description' => 'for designers and copywriters',
      'img' => '/dist/img/thumbnail/livewire.png',
      'img_alt' => 'Illustration showing how a content team, client and creative team collaborate on one document.'
    ),  

    array(
      'title' => 'What&rsquo;s in a Wireframe?',
      'url' => 'https://wearelighthouse.com/blog/whats-in-a-wireframe/',
      'meta_key' => 'Design Article',
      'meta_description' => 'for Lighthouse London',
      'img' => '/dist/img/thumbnail/whats-in-a-wireframe.jpg',
      'img_alt' => 'Wireframe of a content managed website layout'
    ), 

  );

?>

<ol class="c-content-feed u-pv-3rem">

  <?php 
  
    foreach ($feed as $item) {

      echo 
        '<li class="">
          <figure class="c-expo">
              <figcaption class="">
                  <dl class="">
                    <dt class="c-expo__title">
                      <a href="' . $item['url'] . '">' . $item['title'] . '</a>
                    </dt>
                    <dd class="c-expo__meta"><span class="u-color-mango">&rarr; ' . $item['meta_key'] . '</span> ' . $item['meta_description'] . '</dd>
                  </dl>
              </figcaption>
              <div class="c-expo__thumbnail">
                <img src="' . $item['img'] . '" class="c-expo__thumbnail-image" alt="' . $item['img_alt'] . '" loading="auto" />
              </div>
          </figure>
        </li>';

    }

  ?>

</ol>